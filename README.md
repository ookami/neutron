# neutron

### Features

- Customizable proton version
- User configuration and local configuration file

### Installation
Just copy the script and configuration to their respective directories

	# cp ./neutron /usr/local/bin/neutron
	$ mkdir $XDG_CONFIG_HOME/neutron
	$ cp ./neutron.conf $XDG_CONFIG_HOME/neutron/
	$ cp ./neutron.desktop $XDG_DATA_HOME/applications/

### Usage:
<pre>

Usage: neutron [-d PATH] [-i PATH] [-p PATH] [-E ENCODING] exe
	-d|--data:     Compatdata folder
	-i|--install:  Steam install path
	-p|--proton:   Proton binary
	-E|--encoding: Encoding
	exe:           Executable path

</pre>
<hr>
<pre>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

</pre>
